// WebFS: Web FileSystem
//
// WebFS allows you to operate on any underlying directory
// with extended configurable rights for users/groups through
// API over HTTP.
//
// To see its command-line arguments run: `webfs -h`
//
// For information about WebFS core packages run: `go doc`
//	- webfs/core/config | rpc | service | storate | util
//
// For information about WebFS microservices as packages run: `go doc`
//	- webfs/authentificater | filer | indexer | permitter | router
//
package main

import (
	"context"
	"flag"
	"os"
	"strings"
	"syscall"

	// wide-used core modules
	"gitlab.com/gluker/webfs/core/config"
	"gitlab.com/gluker/webfs/core/rpc"
	"gitlab.com/gluker/webfs/core/util"

	// each runs its init() and connects shared data in this order:
	_ "gitlab.com/gluker/webfs/authentificater"

	_ "gitlab.com/gluker/webfs/permitter"

	_ "gitlab.com/gluker/webfs/filer"

	_ "gitlab.com/gluker/webfs/indexer"

	// storages' core and datas
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/auth"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/perm"

	// HTTP to RPC router
	"gitlab.com/gluker/webfs/router"
)

import (
	//#include <unistd.h>
	//#include <errno.h>
	//typedef unsigned int __gid_t;
	//typedef unsigned int __uid_t;
	"C"
)

// Execute real main() after all packages are pre-initialized and linked
func main() {
	// Parse command-line, config and context initialization
	port := flag.Int("port", 8000, "port to bind to")
	path := flag.String("path", ".", "define path to serve files from")
	index := flag.Bool("index", false, "create new index for path")
	uid := flag.Int("uid", 0, "run with user id")
	gid := flag.Int("gid", 0, "run with group id")
	disable_chroot := flag.Bool("disable-chroot", false,
		"disable chroot to path")
	flag.Parse()

	// Initialize config and add there command-line options
	config.Init()
	config.SetValue("Port", *port)
	config.SetValue("Path", strings.TrimRight(*path, "/"))
	config.SetValue("Index", *index)
	util.Verbose(config.GetString("Name"), " ", config.GetString("Version"))

	// chroot to path
	if !*disable_chroot {
		err := syscall.Chroot(config.GetString("Path"))
		util.FatalError(err)

		err = os.Chdir("/")
		util.FatalError(err)

		config.SetValue("Path", "")
		config.SetValue("Chroot", true)
	}

	// set group id
	if *gid != 0 {
		cerr, errno := C.setgid(C.__gid_t(*gid))
		if cerr != 0 {
			util.FatalError(util.Error(
				"Unable to set GID due to error: ", errno))
		}
	}

	// set user id
	if *uid != 0 {
		cerr, errno := C.setuid(C.__uid_t(*uid))
		if cerr != 0 {
			util.FatalError(util.Error(
				"Unable to set UID due to error: ", errno))
		}
	}

	// Initialize context (not used currently, reserved for future)
	ctx := context.Background()

	// Initialize storages
	storage.Init()
	file.Init()
	auth.Init()
	perm.Init()

	// Initialize RPC server (for services) and client (for router)
	done, quit := make(chan bool), make(chan bool)
	go rpc.RPC(ctx, done, quit)
	<-done

	// HTTP Router
	router.Run(ctx)
}
