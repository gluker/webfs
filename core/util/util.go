package util

import (
	"bufio"
	"bytes"
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"runtime"
	"strconv"
	"strings"

	"gitlab.com/gluker/webfs/core/config"
)

func init() {
	config.Init()
}

func FullTrace(depth int) {
	pc := make([]uintptr, depth)
	for i := 0; i < depth; i++ {
		runtime.Callers(i, pc)
		f := runtime.FuncForPC(pc[i])
		file, line := f.FileLine(pc[0])
		fmt.Println(i, file, line, f.Name())
	}
}

func Trace(pos int) string {
	pc := make([]uintptr, 1)
	runtime.Callers(pos, pc)
	f := runtime.FuncForPC(pc[0])

	return f.Name()
}

func utilCaller() string {
	_, file, line, _ := runtime.Caller(2)
	path := strings.Split(file, "/")

	return fmt.Sprintf("%s/%s:%d:",
		path[len(path)-2], path[len(path)-1], line)
}

func Atoi(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}

func Itoa(i int) string {
	return strconv.Itoa(i)
}

func Error(a ...interface{}) error {
	//a = append([]interface{}{"Error: "}, a...)
	return errors.New(fmt.Sprint(a...))
}

func CheckError(err error) bool {
	if err != nil {
		log.Println(utilCaller(), err)
		return false
	}

	return true
}

func FatalError(err error) {
	if err != nil {
		log.Fatalln(utilCaller(), err)
	}
}

func LogError(err ...interface{}) {
	err = append([]interface{}{"Error:"}, err...)
	if err[1] != nil {
		Log(err...)
	} else {
		if len(err) > 2 && err[2] != "" {
			err = append(err[:1], err[2:]...)
			Log(err...)
		}
	}
}

func Log(a ...interface{}) {
	log.Println(a...)
}

func Sprint(a ...interface{}) string {
	return fmt.Sprint(a...)
}

func Sprintf(format string, a ...interface{}) string {
	return fmt.Sprintf(format, a...)
}

func Print(a ...interface{}) {
	fmt.Print(a...)
}

func Println(a ...interface{}) {
	fmt.Println(a...)
}

func PrintStruct(i interface{}) {
	s, _ := json.MarshalIndent(i, "", "\t")
	fmt.Println(string(s))
}

func Debugln(a ...interface{}) {
	debug, err := config.GetBoolErr("Debug")
	if err == nil && debug {
		fmt.Print("### ")
		fmt.Println(a...)
	}
}

func Verbose(a ...interface{}) {
	verbose, err := config.GetBoolErr("Verbose")
	if err == nil && verbose {
		fmt.Print(a...)
	}
}

func ClearString() string {
	return strings.Repeat("\b", 80) + strings.Repeat(" ", 80) +
		strings.Repeat("\b", 80)
}

func Buf(a ...interface{}) string {
	var buf bytes.Buffer

	fmt.Fprint(&buf, a...)

	return buf.String()
}

func Bufln(a ...interface{}) string {
	var buf bytes.Buffer

	fmt.Fprintln(&buf, a...)

	return buf.String()
}

func ScannerPrint(reader io.Reader) {
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func RandomData(length int) (s string, err error) {
	b := make([]byte, length / 2)
	_, err = rand.Read(b)
	if err != nil {
		return
	}
	s = fmt.Sprintf("%x", b)
	return
}
