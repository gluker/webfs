package config

import (
	"errors"
	"fmt"
	"reflect"
	"sync"
)

type ConcurrentConfig struct {
	sync.RWMutex
	c map[string]interface{}
}

var Config *ConcurrentConfig

func NewConcurrentConfig() *ConcurrentConfig {
	return &ConcurrentConfig{
		c: make(map[string]interface{}),
	}
}

func (c *ConcurrentConfig) Load(key string) interface{} {
	c.RLock()
	defer c.RUnlock()
	value, _ := c.c[key]
	return value
}

func (c *ConcurrentConfig) Store(key string, value interface{}) {
	c.Lock()
	defer c.Unlock()
	c.c[key] = value
}

func Init() {
	Config = NewConcurrentConfig()

	defaults := map[string]interface{}{
		"Name":            "WebFS",
		"Version":         "0.37",
		"Debug":           false,
		"Verbose":         true,
		"authentificater": "127.0.0.1:10001",
		"permitter":       "127.0.0.1:10002",
		"filer":           "127.0.0.1:10003",
		"SqlDriver":       "sqlite3",
		"SqlDSN":          "webfs.db?cache=shared&mode=wrc",
		//"SqlDriver":       "postgres",
		//"SqlDSN":          "host=127.0.0.1 port=5432 user=postgres dbname=webfs sslmode=disable",
		//"SqlDriver":       "mysql",
		//"SqlDSN":          "root:mini-pass@tcp(127.0.0.1:3306)/webfs?charset=utf8&parseTime=True",
	}

	for name, value := range defaults {
		Config.Store(name, value)
	}
}

func GetValue(key string) interface{} {
	return Config.Load(key)
}

func SetValue(key string, value interface{}) {
	Config.Store(key, value)
}

func GetString(key string) string {
	if s, ok := Config.Load(key).(string); ok {
		return s
	} else {
		return ""
	}
}

func GetStringErr(key string) (string, error) {
	if s, ok := Config.Load(key).(string); ok {
		return s, nil
	} else {
		return "", errors.New(fmt.Sprint("error converting '", key,
			"' with type ", reflect.TypeOf(Config.Load(key)), " to string"))
	}
}

func GetBool(key string) bool {
	if b, ok := Config.Load(key).(bool); ok {
		return b
	} else {
		return false
	}
}

func GetBoolErr(key string) (bool, error) {
	if b, ok := Config.Load(key).(bool); ok {
		return b, nil
	} else {
		return false, errors.New(fmt.Sprint("error converting '", key,
			"' with type ", reflect.TypeOf(Config.Load(key)), " to bool"))
	}
}

func GetInt(key string) int {
	if i, ok := Config.Load(key).(int); ok {
		return i
	} else {
		return 0
	}
}

func GetIntErr(key string) (int, error) {
	if i, ok := Config.Load(key).(int); ok {
		return i, nil
	} else {
		return 0, errors.New(fmt.Sprint("error converting '", key,
			"' with type ", reflect.TypeOf(Config.Load(key)), " to int"))
	}
}
