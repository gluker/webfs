package service

import "context"

// local (direct) API for service to export
type LocalAPI interface {
	Init(ctx context.Context) error
}

// remote (RPC) API for service
type RemoteAPI interface{}

// Container for service: name and interfaces to local/remote API
type ServiceType struct {
	Name   string
	Local  LocalAPI
	Remote RemoteAPI
}

// Slice for containers
type ContainerType struct {
	Services []ServiceType
}

var Container *ContainerType

func init() {
	Container = &ContainerType{[]ServiceType{}}
}

func Register(name string, local LocalAPI, remote RemoteAPI) {
	Container.Services = append(Container.Services,
		ServiceType{name, local, remote})
}
