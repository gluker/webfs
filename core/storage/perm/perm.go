package perm

// Separate type for permissions to ease operations
type Perm int

const (
	Read Perm = iota + 1
	Create
	Modify
	Delete
	List
	Permit
	Full
)
