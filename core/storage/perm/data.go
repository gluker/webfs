package perm

import "gitlab.com/gluker/webfs/core/storage"

// Available permissions for file objects (files)
type Permission struct {
	Id   int    `gorm:"primary_key"`
	Name string `gorm:"index; unique_index"`
}

// Users' permissions for files
type UserFilePermission struct {
	Id           int `gorm:"primary_key"`
	UserId       int `gorm:"index; unique_index:uix_user_file_permission" sql:"type:int REFERENCES \"user\"(id) ON UPDATE CASCADE ON DELETE CASCADE"`
	FileId       int `gorm:"index; unique_index:uix_user_file_permission" sql:"type:int REFERENCES \"file\"(id) ON UPDATE CASCADE ON DELETE CASCADE"`
	PermissionId int `gorm:"index; unique_index:uix_user_file_permission" sql:"type:int REFERENCES \"permission\"(id) ON UPDATE CASCADE ON DELETE CASCADE"`
}

// Groups' permissions for files
type GroupFilePermission struct {
	Id           int `gorm:"primary_key"`
	GroupId      int `gorm:"index; unique_index:uix_group_file_permission" sql:"type:int REFERENCES \"group\"(id) ON UPDATE CASCADE ON DELETE CASCADE"`
	FileId       int `gorm:"index; unique_index:uix_group_file_permission" sql:"type:int REFERENCES \"file\"(id) ON UPDATE CASCADE ON DELETE CASCADE"`
	PermissionId int `gorm:"index; unique_index:uix_group_file_permission" sql:"type:int REFERENCES \"permission\"(id) ON UPDATE CASCADE ON DELETE CASCADE"`
}

func Init() {
	storage.Db.AutoMigrate(
		&Permission{},
		&UserFilePermission{},
		&GroupFilePermission{})

	storage.Db.Model(&UserFilePermission{}).
		AddForeignKey("user_id", "\"user\"(id)", "CASCADE", "CASCADE").
		AddForeignKey("file_id", "\"file\"(id)", "CASCADE", "CASCADE").
		AddForeignKey("permission_id", "\"permission\"(id)", "CASCADE", "CASCADE")

	storage.Db.Model(&GroupFilePermission{}).
		AddForeignKey("group_id", "\"group\"(id)", "CASCADE", "CASCADE").
		AddForeignKey("file_id", "\"file\"(id)", "CASCADE", "CASCADE").
		AddForeignKey("permission_id", "\"permission\"(id)", "CASCADE", "CASCADE")
}

// Separate type for permissions to ease operations
/*
type Perm int

const (
	Read Perm = iota + 1
	Create
	Modify
	Delete
	List
	Permit
	Full
)
*/

func (perm Perm) Len() int {
	return len(_Perm_index)
}

func (perm Perm) Int() int {
	return int(perm)
}

func (perm Perm) Permission() Permission {
	return Permission{Id: int(perm), Name: perm.String()}
}

type Data struct {
	Permission []Permission
	User       []UserFilePermission
	Group      []GroupFilePermission
}
