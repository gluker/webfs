package storage

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"gitlab.com/gluker/webfs/core/config"
	"gitlab.com/gluker/webfs/core/util"
)

var Db *gorm.DB // Global DB query handler
var SqlDriver string

func Init() {
	var err error

	// Set low-level DB driver
	SqlDriver, err = config.GetStringErr("SqlDriver")
	util.FatalError(err)

	// Open DB
	Db, err = gorm.Open(SqlDriver, config.GetString("SqlDSN"))
	util.FatalError(err)

	// SQL Query Logging for debug purpose
	Db.LogMode(config.GetBool("Debug"))

	// let GORM create table name 'user' instead of 'users'
	Db.SingularTable(true)

	// sqlite needs it to keep ON UPDATE / ON DELETE statements
	if SqlDriver == "sqlite3" {
		err = Db.Exec("PRAGMA foreign_keys = ON").Error
		util.FatalError(err)
		err = Db.Exec("PRAGMA busy_timeout = 1000").Error
		util.FatalError(err)
		err = Db.Exec("PRAGMA busy_timeout = 1000").Error
		util.FatalError(err)
		Db.DB().SetMaxIdleConns(10)
		Db.DB().SetMaxOpenConns(100)
	}

	// mysql needs it to be compatible to create table and indexes
	if SqlDriver == "mysql" {
		err = Db.Exec("SET GLOBAL sql_mode = 'ANSI_QUOTES'").Error
		util.FatalError(err)

		err = Db.Exec("SET SESSION sql_mode = 'ANSI_QUOTES'").Error
		util.FatalError(err)
	}
}
