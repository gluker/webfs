package file

import (
	"bytes"

	"gitlab.com/gluker/webfs/core/config"
	"gitlab.com/gluker/webfs/core/storage"
)

// DB table 'file' stores files & directories information

type File struct {
	Id       int	`gorm:"primary_key" sql: "auto_increment; primary_key; unique"`
	ParentId int	`gorm:"index; unique_index:uix_parentid_name" sql:"type:int REFERENCES \"file\"(id) ON UPDATE CASCADE ON DELETE CASCADE"`
	Name     string `gorm:"index; unique_index:uix_parentid_name"`
	Size     int64
	IsDir    bool
}

func Init() {
	storage.Db.AutoMigrate(&File{})
	storage.Db.Model(&File{}).AddForeignKey(
		"parent_id", "\"file\"(id)", "CASCADE", "CASCADE")
}

// Separate type for slice of files to define its methods
type Data struct {
	Path []File
}

// Get parents for FileId
func (d *Data) Build() (err error) {
	id := (*d).Path[0].Id
	if id == 0 {
		id = (*d).Path[0].ParentId
	}

	for {
		if err != nil || id <= 1 {
			break
		}

		var f File

		err = storage.Db.Where("id = ?", &id).Find(&f).Error
		(*d).Path = append((*d).Path, f)
		id = f.ParentId
	}

	if (*d).Path[0].ParentId == 0 && len((*d).Path) > 1 {
		(*d).Path = append((*d).Path[:0], (*d).Path[1:]...)
	}

	if (*d).Path[0].Id == 0 && len((*d).Path) > 1 {
		(*d).Path[0].Id = d.Path[1].Id
	}

	return err
}

// Path in filesystem for FileId
func (d *Data) Location() (str string) {
	var buffer bytes.Buffer

	buffer.WriteString(config.GetString("Path"))
	for i := len((*d).Path); i > 0; i-- {
		buffer.WriteString("/" + (*d).Path[i-1].Name)
	}

	return buffer.String()
}

/*
type Path []File

// Get parents for FileId
func (p *Path) Build() (err error) {
	id := (*p)[0].Id
	if id == 0 {
		id = (*p)[0].ParentId
	}

	for {
		if err != nil || id <= 1 {
			break
		}

		var f File

		err = storage.Db.Where("id = ?", &id).Find(&f).Error
		(*p) = append((*p), f)
		id = f.ParentId
	}

	if (*p)[0].ParentId == 0 && len((*p)) > 1 {
		(*p) = append((*p)[:0], (*p)[1:]...)
	}

	return err
}

// Path in filesystem for FileId
func (p *Path) Location() string {
	var buffer bytes.Buffer

	buffer.WriteString(config.GetString("Path"))
	for i := len((*p)); i > 0; i-- {
		buffer.WriteString("/" + (*p)[i-1].Name)
	}

	return buffer.String()
}
*/
