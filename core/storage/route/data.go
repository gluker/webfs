package route

import (
	"gitlab.com/gluker/webfs/core/storage/auth"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/perm"
)

type FunctionData struct {
	Name       string
	Permission []perm.Perm
}

type Options struct {
	Recursive bool
	Offset    int
	Limit     int
	Data      []byte
}

type Request struct {
	Auth    *auth.Data // current's user full information
	User    auth.User
	Group   auth.Group
	File    []*file.Data
	Perm    []perm.Permission
	Options Options
}
