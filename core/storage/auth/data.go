package auth

import "gitlab.com/gluker/webfs/core/storage"

// DB table 'user' stores login/password
type User struct {
	Id       int    `gorm:"primary_key"`
	Name     string `gorm:"index; unique_index"`
	Password string
}

// DB table 'group' stores groups
type Group struct {
	Id   int    `gorm:"primary_key"`
	Name string `gorm:"index; unique_index"`
}

// DB table 'user_group': one user to many group
type UserGroup struct {
	Id      int `gorm:"primary_key"`
	UserId  int `gorm:"index; unique_index:uix_user_group" sql:"type:int REFERENCES \"user\"(id) ON UPDATE CASCADE ON DELETE CASCADE"`
	GroupId int `gorm:"index; unique_index:uix_user_group" sql:"type:int REFERENCES \"group\"(id) ON UPDATE CASCADE ON DELETE CASCADE"`
}

func Init() {
	storage.Db.AutoMigrate(&User{}, &Group{}, &UserGroup{})
	storage.Db.Model(&UserGroup{}).
		AddForeignKey("user_id", "\"user\"(id)", "CASCADE", "CASCADE").
		AddForeignKey("group_id", "\"group\"(id)", "CASCADE", "CASCADE")
}

// Store user's info, groups and token
type Data struct {
	User  User    // User data
	Group []Group // []Group data
	Token string  // token
}

// Store group's info including users in it
type GroupData struct {
	Group Group  // User data
	User  []User // []Group data
}
