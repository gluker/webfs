package rpc

import (
	"context"

	"github.com/valyala/gorpc"

	"gitlab.com/gluker/webfs/core/config"
	"gitlab.com/gluker/webfs/core/service"
	"gitlab.com/gluker/webfs/core/util"
)

var (
	Dispatcher   map[string]*gorpc.Dispatcher
	RemoteClient map[string]*gorpc.DispatcherClient
)

func RPC(ctx context.Context, done chan<- bool, quit <-chan bool) error {
	Dispatcher = make(map[string]*gorpc.Dispatcher)
	RemoteClient = make(map[string]*gorpc.DispatcherClient)

	sync := make(chan bool) // local loop synchronization

	for _, service := range service.Container.Services {
		go func() {
			util.Verbose("\n*** ", service.Name)
			util.FatalError(service.Local.Init(ctx))

			// if service has no RPC address it means only local init (indexer)
			serviceAddr := config.GetString(service.Name)
			if serviceAddr == "" {
				sync <- true
				return
			}

			Dispatcher[service.Name] = gorpc.NewDispatcher()
			Dispatcher[service.Name].AddService(service.Name, service.Remote)

			server := gorpc.NewTCPServer(serviceAddr,
				Dispatcher[service.Name].NewHandlerFunc())
			util.FatalError(server.Start())
			defer server.Stop()

			client := gorpc.NewTCPClient(serviceAddr)
			client.Start()
			defer client.Stop()

			RemoteClient[service.Name] =
				Dispatcher[service.Name].NewServiceClient(service.Name, client)

			sync <- true // let loop continue
			<-quit       // global chan for all goroutines to keep them running
		}()

		<-sync // waiting for goroutine to run
	}

	done <- true // let main thread continue

	return nil
}
