package permitter

import (
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/auth"
	"gitlab.com/gluker/webfs/core/storage/perm"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func prepareRequest(req *route.Request) (err error) {
	if req.Perm[0].Id == 0 {
		err = storage.Db.Where("name = ?", req.Perm[0].Name).
			Find(&req.Perm).Error
		if !util.CheckError(err) {
			return err
		}
		if len(req.Perm) == 0 || req.Perm[0] == (perm.Permission{}) {
			return util.Error("incorrect permission")
		}
	}

	if req.Group == (auth.Group{}) {
		if req.User.Id == 0 {
			err = storage.Db.Where("name = ?", req.User.Name).
				Find(&req.User).Error
			util.CheckError(err)
		}
	} else {
		if req.Group.Id == 0 {
			err = storage.Db.Where("name = ?", req.Group.Name).
				Find(&req.Group).Error
			util.CheckError(err)
		}
	}

	return err
}

func (r *PermitterRemote) Give(req *route.Request) (null, err error) {
	if err = prepareRequest(req); err != nil {
		return null, err
	}

	if req.Group == (auth.Group{}) {
		ufp := perm.UserFilePermission{UserId: req.User.Id,
			FileId: req.File[0].Path[0].Id, PermissionId: req.Perm[0].Id}
		err = storage.Db.Save(&ufp).Error
	} else {
		gfp := perm.GroupFilePermission{GroupId: req.Group.Id,
			FileId: req.File[0].Path[0].Id, PermissionId: req.Perm[0].Id}
		err = storage.Db.Save(&gfp).Error
	}

	util.CheckError(err)
	return null, err
}

func (r *PermitterRemote) Take(req *route.Request) (null, err error) {
	if err = prepareRequest(req); err != nil {
		return null, err
	}

	if req.Group == (auth.Group{}) {
		err = storage.Db.Delete(&perm.UserFilePermission{},
			"user_id = ? AND file_id = ? AND permission_id = ?",
			req.User.Id, req.File[0].Path[0].Id, req.Perm[0].Id).Error
	} else {
		err = storage.Db.Delete(&perm.GroupFilePermission{},
			"group_id = ? AND file_id = ? AND permission_id = ?",
			req.Group.Id, req.File[0].Path[0].Id, req.Perm[0].Id).Error
	}

	util.CheckError(err)
	return null, err
}
