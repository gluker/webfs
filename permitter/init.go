package permitter

import (
	"context"

	"gitlab.com/gluker/webfs/core/service"
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/perm"
	"gitlab.com/gluker/webfs/core/util"
)

type PermitterLocal struct{}
type PermitterRemote struct{}

func init() {
	service.Register("permitter", &PermitterLocal{}, &PermitterRemote{})
}

// create or load DB tables
func (l *PermitterLocal) Init(ctx context.Context) error {
	// check if permissions are set: if not -- set them up
	var permissions []perm.Permission
	err := storage.Db.Find(&permissions).Error
	util.FatalError(err)

	if len(permissions) < 1 {
		var i perm.Perm
		for i = 1; i < perm.Perm(i.Len()); i++ {
			err := storage.Db.Save(
				&perm.Permission{Id: int(i), Name: i.String()}).Error
			util.FatalError(err)
		}
	}

	return nil
}
