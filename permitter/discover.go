package permitter

import (
	"gitlab.com/gluker/webfs/core/storage/perm"
	"gitlab.com/gluker/webfs/core/storage/route"
)

func (r *PermitterRemote) Discover(i int) (
	fd []route.FunctionData, err error) {

	fd = append(fd, route.FunctionData{"Show", []perm.Perm{perm.Permit}})
	fd = append(fd, route.FunctionData{"Give", []perm.Perm{perm.Permit}})
	fd = append(fd, route.FunctionData{"Take", []perm.Perm{perm.Permit}})

	return fd, nil
}
