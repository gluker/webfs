package permitter

import (
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

type DataShow struct {
	User       string
	Group      string
	Permission string
}

func (r *PermitterRemote) Show(req *route.Request) ([]DataShow, error) {
	var ret []DataShow

	rows, err := storage.Db.Table("user_file_permission").
		Joins("JOIN \"user\" ON \"user\".id = "+
			"user_file_permission.user_id").
		Joins("JOIN permission ON permission.id = "+
			"user_file_permission.permission_id").
		Where("user_file_permission.file_id = ?", req.File[0].Path[0].Id).
		Select("\"user\".name, permission.name").Rows()
	defer rows.Close()

	if !util.CheckError(err) {
		return nil, err
	}

	for rows.Next() {
		ds := DataShow{}
		rows.Scan(&ds.User, &ds.Permission)
		ret = append(ret, ds)
	}

	rows, err = storage.Db.Table("group_file_permission").
		Joins("JOIN \"group\" ON \"group\".id = "+
			"group_file_permission.group_id").
		Joins("JOIN permission ON permission.id = "+
			"group_file_permission.permission_id").
		Where("group_file_permission.file_id = ?", req.File[0].Path[0].Id).
		Select("\"group\".name, permission.name").Rows()
	defer rows.Close()

	if !util.CheckError(err) {
		return nil, err
	}

	for rows.Next() {
		ds := DataShow{}
		rows.Scan(&ds.Group, &ds.Permission)
		ret = append(ret, ds)
	}

	return ret, nil
}
