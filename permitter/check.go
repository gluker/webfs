package permitter

import (
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/auth"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/perm"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func permit(permissions *[]perm.Permission, p perm.Perm) bool {
	if p.String() == "Read" { // for Read any Permission is true
		for _, permission := range *permissions {
			if permission.Id >= int(p) {
				return true
			}
		}
	} else { // perm.Perm must be equal to Permission or Permission is Full
		for _, permission := range *permissions {
			if permission.Id == int(p) || permission.Name == "Full" {
				return true
			}
		}
	}

	return false
}

func CheckFile(f *file.File, authData *auth.Data, p perm.Perm) (err error) {
	var permissions []perm.Permission
	var hasPermission bool

	// user 'admin' is allowed to do anything by default 
	if authData.User.Name == "admin" {
		return nil
	}

	err = storage.Db.Joins("JOIN user_file_permission ON "+
		"user_file_permission.permission_id = permission.id").
		Where("user_file_permission.file_id = ?", f.Id).
		Where("user_file_permission.user_id = ?", authData.User.Id).
		Find(&permissions).Error
	if !util.CheckError(err) {
		return err
	}
	if permit(&permissions, p) {
		return nil
	}

	if len(permissions) > 0 {
		hasPermission = true
	}

	var groupIds []int
	for _, group := range authData.Group {
		// any user in group 'admin' is allowed to do anything by default
		if group.Name == "admin" {
			return nil
		}

		groupIds = append(groupIds, group.Id)
	}

	err = storage.Db.Joins("JOIN group_file_permission ON "+
		"group_file_permission.permission_id = permission.id").
		Where("group_file_permission.file_id = ?", f.Id).
		Where("group_file_permission.group_id IN (?)", groupIds).
		Find(&permissions).Error
	if !util.CheckError(err) {
		return err
	}
	if permit(&permissions, p) {
		return nil
	}

	// if file has no permissions then Read or List are allowed for everyone
	if !hasPermission && len(permissions) == 0 &&
		(p == perm.Read || p == perm.List) {
		return nil
	}

	return util.Error("operation '", p.String(), "' not allowed for id: ", f.Id)
}

func CheckPath(path *[]file.File, authData *auth.Data) (err error) {
	for i := 1; i < len(*path); i++ {
		err = CheckFile(&(*path)[i], authData, perm.Read)
		if !util.CheckError(err) {
			return err
		}
	}

	return nil
}

func (r *PermitterRemote) Check(req *route.Request) (err error) {
	if len(req.File) == 0 || len(req.File) != len(req.Perm) {
		return util.Error("invalid length of elements in File/Perm")
	}

	for i := 0; i < len(req.File); i++ {
		if err = CheckFile(&req.File[i].Path[0], req.Auth,
			perm.Perm(req.Perm[i].Id)); err != nil {
			return err
		}

		if len(req.File[i].Path) > 1 {
			if err = CheckPath(&req.File[i].Path, req.Auth); err != nil {
				return err
			}
		}
	}

	return nil
}
