#!/bin/bash

cd `dirname $0`

function usage {
    cat << EOF
Usage: `basename $0` [ARGUMENTS]

This script will build Docker image to run webfs

Arguments:
    -h, --help              show this help message and exit
    -p, --port [PORT]       port for webfs to bind to (default: $PORT)
    -r, --root [DIRECTORY]  root directory for serving files from
    -n, --no-index          don't index files in root
    -c, --clear             clear intermediate/development images
    -s, --skip              skip build development image

EOF
    exit 1
}


function webfs-dev {
    docker rmi webfs-dev &> /dev/null

    cat > Dockerfile << EOF 
FROM golang:1.15-alpine3.12 AS webfs-dev
LABEL stage=webfs-dev
RUN mkdir -p /go/src/gitlab.com/gluker/webfs/
ADD . /go/src/gitlab.com/gluker/webfs/
WORKDIR /go/src/gitlab.com/gluker/webfs/
RUN apk add --no-cache git gcc libc-dev upx
RUN go get -v
RUN go build -a -v -o /webfs \
    -ldflags "-linkmode external -extldflags '-static' -s -w"
RUN upx -9 /webfs
EOF

    if ! docker build -t webfs-dev . ; then
        exit 1
    fi

    rm Dockerfile
}


function webfs {
    docker kill webfs &> /dev/null

    docker rmi webfs &> /dev/null

    cat > Dockerfile << EOF
FROM scratch AS webfs
COPY --from=webfs-dev /webfs /webfs
EXPOSE $PORT
ENTRYPOINT ["/webfs", "$INDEX", "-path", "storage", \
    "-port", "$PORT", "disable-chroot"]
EOF

    docker build -t webfs .

    rm Dockerfile

    docker run  --detach --rm --publish $PORT:$PORT     \
                --volume "$ROOT":/storage               \
                --name webfs webfs
}


INDEX="-index"
PORT=8000

for arg in "$@" ; do
    case $arg in
        -p|--port)
            PORT="$2"
            shift
            shift
            ;;
        -r|--root)
            ROOT=`realpath "$2"`
            shift
            shift
            ;;
        -n|--no-index)
            INDEX=""
            shift
            ;;
        -c|--clear)
            CLEAR=true
            shift
            ;;
        -s|--skip)
            SKIP=true
            shift
            ;;
        -h|--help)
            usage
            ;;
    esac
done

if [ ! "$ROOT" ] ; then
    usage
elif [ ! -d "$ROOT" ] ; then
    echo -e "ERROR: directory \"$ROOT\" doesn't exist\n"
    usage
fi


if [ ! $SKIP ] ; then
    webfs-dev
fi

webfs

if [ $CLEAR ] ; then
    docker image prune --force --filter label=stage=webfs-dev
    docker rmi webfs-dev
fi

