package router

import (
	"sync"

	"gitlab.com/gluker/webfs/core/rpc"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func Discover() {
	var wg sync.WaitGroup

	for serviceName, _ := range rpc.Dispatcher {
		wg.Add(1)
		go func(s string) {
			defer wg.Done()
			x, err := rpc.RemoteClient[s].Call("Discover", 0)
			util.FatalError(err)
			Route.Add(s, x.([]route.FunctionData))
		}(serviceName)
	}

	wg.Wait()
}
