package router

import (
	"net/http"
	"reflect"
	"time"

	"gitlab.com/gluker/webfs/core/rpc"
	"gitlab.com/gluker/webfs/core/storage/auth"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/perm"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func Login(w http.ResponseWriter, r *http.Request) {
	req, err := JsonRequest(w, r, "")
	if err != nil {
		return
	}

	if req.Auth == nil {
		err = util.Error("invalid request")
		util.CheckError(err)
		JsonResponse(w, nil, err)
		return
	}

	x, err := rpc.RemoteClient["authentificater"].Call("Login", &req.Auth.User)
	if !util.CheckError(err) {
		JsonResponse(w, nil, err)
		return
	}

	req.Auth = x.(*auth.Data)
	cookie := http.Cookie{Name: "token", Value: req.Auth.Token, Path: "/"}
	http.SetCookie(w, &cookie)
	JsonResponse(w, nil, nil)
}

func Logout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		MaxAge:  -1,
		Path:    "/",
		Expires: time.Now().Add(-100 * time.Hour)})
}

func Token(w http.ResponseWriter, r *http.Request) (*auth.Data, error) {
	cookie, err := r.Cookie("token")
	if !util.CheckError(err) {
		JsonResponse(w, nil, err)
		return nil, err
	}

	x, err := rpc.RemoteClient["authentificater"].Call("Token", cookie.Value)
	if !util.CheckError(err) {
		JsonResponse(w, nil, err)
		return nil, err
	}

	return x.(*auth.Data), nil
}

func FileData(w http.ResponseWriter, req *route.Request) ([]*file.Data, error) {
	if len(req.File) == 0 {
		return nil, nil
	}

	x, err := rpc.RemoteClient["filer"].Call("FileData", req)
	if !util.CheckError(err) {
		JsonResponse(w, nil, err)
		return nil, err
	}

	return x.(*route.Request).File, nil
}

// checks if user is "admin" or in "admin" group
func hasPrivilege(req *route.Request) bool {
	if req.Auth.User.Name == "admin" {
		return true
	}

	for _, group := range req.Auth.Group {
		if group.Name == "admin" {
			return true
		}
	}

	return false
}

func PermCheck(w http.ResponseWriter, req *route.Request,
	permission []perm.Perm) error {

	// remote functions (authentificator/*) come with no Perm
	// but available only to admin group
	if len(permission) == 0 {
		if !hasPrivilege(req) {
			err := util.Error("insufficient privileges")
			JsonResponse(w, nil, err)
			return err
		} else {
			return nil
		}
	}

	var nreq route.Request
	nreq.Auth = req.Auth
	nreq.File = req.File
	nreq.Perm = make([]perm.Permission, len(permission))
	for i, _ := range nreq.Perm {
		nreq.Perm[i].Id = permission[i].Int()
		nreq.Perm[i].Name = permission[i].String()
	}

	_, err := rpc.RemoteClient["permitter"].Call("Check", &nreq)
	if !util.CheckError(err) {
		JsonResponse(w, nil, err)
	}

	return err
}

func RemoteCall(w http.ResponseWriter, req *route.Request,
	serviceName string, functionName string) (*route.Request, error) {

	x, err := rpc.RemoteClient[serviceName].Call(functionName, req)
	util.CheckError(err)
	JsonResponse(w, x, err)

	if reflect.TypeOf(x) == reflect.TypeOf(&route.Request{}) {
		return x.(*route.Request), err
	} else {
		return nil, err
	}
}
