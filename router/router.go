package router

import (
	"context"
	"net/http"
	"strings"
	"sync"
	"unicode"

	"github.com/gorilla/mux"

	"gitlab.com/gluker/webfs/core/config"
	"gitlab.com/gluker/webfs/core/storage/perm"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

type RouteServiceFunction struct {
	sync.RWMutex
	Service map[string]map[string][]perm.Perm
}

var Route = &RouteServiceFunction{
	Service: make(map[string]map[string][]perm.Perm)}

func (rt *RouteServiceFunction) Add(serviceName string,
	functionData []route.FunctionData) {

	rt.Lock()
	defer rt.Unlock()

	function, ok := rt.Service[serviceName]
	if !ok {
		function = make(map[string][]perm.Perm)
		rt.Service[serviceName] = function
	}

	for _, fd := range functionData {
		function[fd.Name] = fd.Permission
	}
}

func (rt *RouteServiceFunction) Wrapper(w http.ResponseWriter, r *http.Request,
	serviceName string, functionName string) {

	var req *route.Request
	var err error

	if req, err = JsonRequest(w, r, functionName); err != nil {
		return
	}

	if req.Auth, err = Token(w, r); err != nil {
		return
	}

	if req.File, err = FileData(w, req); err != nil {
		return
	}

	if PermCheck(w, req, Route.Service[serviceName][functionName]) != nil {
		return
	}

	req, _ = RemoteCall(w, req, serviceName, functionName)

	if strings.Contains(functionName, "Download") {
		Download(w, req)
	}
}

func func2url(s string) (x string) {
	for i := 0; i < len(s); i++ {
		if i != 0 && unicode.IsUpper(rune(s[i])) {
			x = x + "-"
		}
		x = x + string(unicode.ToLower(rune(s[i])))
	}

	return x
}

func Run(ctx context.Context) error {
	Discover()

	router := mux.NewRouter()
	router.HandleFunc("/authentificater/login", Login)
	router.HandleFunc("/authentificater/logout", Logout)

	Route.RLock()
	for serviceName, serviceMap := range Route.Service {
		for functionName, _ := range serviceMap {
			servName, funcName := serviceName, functionName
			router.HandleFunc("/"+servName+"/"+func2url(funcName),
				func(w http.ResponseWriter, r *http.Request) {
					Route.Wrapper(w, r, servName, funcName)
				})
		}
	}
	Route.RUnlock()

	util.FatalError(http.ListenAndServe(
		":" + util.Itoa(config.GetInt("Port")), router))

	return nil
}
