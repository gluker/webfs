package router

import (
	"encoding/json"
	"net/http"
	"reflect"
	"strings"

	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

type Message struct {
	Data  []interface{}
	Error string
}

func JsonRequest(w http.ResponseWriter, r *http.Request, functionName string) (
	*route.Request, error) {

	var req route.Request
	var err error

	defer r.Body.Close()

	if strings.Contains(functionName, "Upload") {
		err = Upload(r, &req)
		if !util.CheckError(err) {
			 JsonResponse(w, nil, "error in upload ", err)
		}
	} else {
		err = json.NewDecoder(r.Body).Decode(&req)
		if !util.CheckError(err) {
			JsonResponse(w, nil, "error in decoding ", err)
		}
	}

	if req.Options.Offset == 0 {
		req.Options.Offset = -1
	}

	if req.Options.Limit == 0 {
		req.Options.Limit = -1
	}

	return &req, err
}

func JsonResponse(w http.ResponseWriter, x interface{}, err ...interface{}) {
	m := Message{}

	if reflect.TypeOf(x) == reflect.TypeOf(&route.Request{}) {
		if len(x.(*route.Request).Options.Data) > 0 {
			return
		}
	} else if reflect.TypeOf(x) != nil {
		s := reflect.ValueOf(x)
		for i := 0; i < s.Len(); i++ {
			m.Data = append(m.Data, s.Index(i).Interface())
		}
	}

	for _, e := range err {
		_err, _ := e.(error)
		if _err != nil {
			m.Error = util.Sprint(m.Error, _err)
		}
	}

	out, _ := json.MarshalIndent(m, "", "  ")
	w.Write(out)
}
