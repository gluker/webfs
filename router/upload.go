package router

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"mime/multipart"

	"gitlab.com/gluker/webfs/core/storage/route"
)

func Upload(r *http.Request, req *route.Request) error {
	mr, err := r.MultipartReader()
	if err != nil {
		return err
	}

	for {
		var part *multipart.Part

		part, err = mr.NextPart()
		if err != nil {
			if err == io.EOF {
				err = nil
			}
			break
		}

		if part.FileName() == "upload.json" {
			err = json.NewDecoder(part).Decode(req)
		} else {
			req.Options.Data, err = ioutil.ReadAll(part)
		}
		if err != nil {
			break
		}
	}

	return err
}
