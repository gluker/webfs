package router

import (
	"net/http"

	"gitlab.com/gluker/webfs/core/storage/route"
)

func Download(w http.ResponseWriter, req *route.Request) {
	if req == nil {
		return
	}

	w.Header().Set("Content-Disposition",		// inline
		"attachment; filename=\"" + req.File[0].Path[0].Name + "\"")
    w.Header().Set("Content-Type", "application/octet-stream")

	w.Write(req.Options.Data)
}
