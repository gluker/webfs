# API
Взаимодействие с WebFS осуществляется через отправку и получение JSON-данных через POST-запрос к URL, сформированному по принципу: ``service``/``function``

## сервисы и функции

### `authentificater`

#### — `Login`: аутентификация пользователя

- URL: `authentificater/login`
- code: `login_token.go`
- cookies: устанавливает аутентификационный токен
- JSON-запрос:

	```
	{
	    "auth": {
	        "user": {
	            "name": "admin",
	            "password": "aaa123"
	        }
	    }
	}

	```
- JSON-ответ: `{ "Data": null, "Error": "" }` 
- утилита: `test/login.sh login admin aaa123`


#### — `Logout`: удаление аутентификационного токена

- URL: `authentificater/logout`
- code: `login_token.go`
- cookies: удаляет аутентификационный токен
- JSON-запрос: отсутствует
- JSON-ответ: отсутствует
- утилита: `test/login.sh logout`


#### — `UserList`: информация о пользователе(-ях) 

- URL: `authentificater/user-list`
- code: `user.go`
- JSON-запрос:

	```
	{
	    "user": {
	        "name": "admin"
	    }
	}
	```
- JSON-ответ:
	
	```
	{
	  "Data": [
	    {
	      "User": {
	        "Id": 1,
	        "Name": "admin",
	        "Password": "$2a$10$wvBtXt4frvhP26K50c4qi.s1DSt4vEndtGHraThCSFFjCPl4iAYb."
	      },
	      "Group": [
	        {
	          "Id": 1,
	          "Name": "admin"
	        },
	        {
	          "Id": 2,
	          "Name": "user"
	        }
	      ],
	      "Token": "28b7c9c0-c985-476f-8888-9255cb0dd46f"
	    }
	  ],
	  "Error": ""
	}
	```
- утилита: `test/auth.sh user-list admin`


#### — `UserAdd`: добавление пользователя

- URL: `authentificater/user-add`
- code: `user.go`
- JSON-запрос:

	```
	{
	    "user": {
	        "name": "test",
	        "password": "password"
	    }
	}

	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/auth.sh user-add test password`


#### — `UserPassword`: изменение пароля пользователя

- URL: `authentificater/user-password`
- code: `user.go`
- JSON-запрос:

	```
	{
	    "user": {
	        "name": "test",
	        "password": "otherpass"
	    }
	}

	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/auth.sh user-password test otherpass`


#### — `UserDelete`: удаление пользователя

- URL: `authentificater/user-delete`
- code: `user.go`
- JSON-запрос:

	```
	{
	    "user": {
	        "name": "test"
	    }
	}

	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/auth.sh user-delete test`


#### — `GroupList`: информация о группе(-ах)

- URL: `authentificater/group-list`
- code: `group.go`
- JSON-запрос:

	```
	{
	    "group": {
	        "name": "admin"
	    }
	}

	```
- JSON-ответ: 

	```
	{
	  "Data": [
	    {
	      "Group": {
	        "Id": 1,
	        "Name": "admin"
	      },
	      "User": [
	        {
	          "Id": 1,
	          "Name": "admin",
	          "Password": "$2a$10$wvBtXt4frvhP26K50c4qi.s1DSt4vEndtGHraThCSFFjCPl4iAYb."
	        }
	      ]
	    }
	  ],
	  "Error": ""
	}
	```
- утилита: `test/auth.sh group-list admin`


#### — `GroupAdd`: добавление группы

- URL: `authentificater/group-add`
- code: `group.go`
- JSON-запрос:

	```
	{
	    "group": {
	        "name": "test"
	    }
	}

	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/auth.sh group-add test`


#### — `GroupDelete`: удаление группы

- URL: `authentificater/group-delete`
- code: `group.go`
- JSON-запрос:

	```
	{
	    "group": {
	        "name": "test"
	    }
	}

	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/auth.sh group-delete test`


#### — `GroupUserAdd`: добавление пользователя в группу

- URL: `authentificater/group-user-add`
- code: `group_user.go`
- JSON-запрос:

	```
	{
	    "group": {
	        "name": "test"
	    },
	    "user": {
	        "name": "test"
	    }
	}
	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/auth.sh group-user-add test test`


#### — `GroupUserDelete`: удаление пользователя из группы

- URL: `authentificater/group-user-delete`
- code: `group_user.go`
- JSON-запрос:

	```
	{
	    "group": {
	        "name": "test"
	    },
	    "user": {
	        "name": "test"
	    }
	}

	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/auth.sh group-user-delete test test`



### `filer`

#### — `Ls`: просмотр файлов в директории

- URL: `filer/ls`
- code: `ls.go`
- JSON-запрос:

	```
	{
	    "file": [
	        {
	            "path": [
	                {
	                    "id": 1
	                } 
	            ]
	        }
	        
	    ],
	    "options": {
	        "limit": 0,
	        "offset": 0
	    }
	}
	```
- JSON-ответ: 

	```
	"Data": [
	    {
	      "Id": 3,
	      "ParentId": 1,
	      "Name": "Manifest.gz",
	      "Size": 1982,
	      "IsDir": false
	    },
	    {
	      "Id": 40384,
	      "ParentId": 1,
	      "Name": "xml-schema",
	      "Size": 9,
	      "IsDir": true
	    }
	  ],
	  "Error": ""
	}
	```
- утилита: `test/filer.sh ls 1`


#### — `Cp`: копирование файлового объекта

- URL: `filer/cp`
- code: `cp.go`
- JSON-запрос:

	```
	{
	    "file": [
	        {
	            "path": [
	                {
	                    "id": 40391
	                } 
	            ]
	        }
	        
	        ,
	        {
	            "path": [
	                {
	                    "parentid": 40384,
	                    "name": "timestamp.xxx"
	                }
	            ]
	            
	        }
	        
	    ]
	}

	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/filer.sh cp 40391 40384 timestamp.xxx`


#### — `Mv`: переименование файлового объекта

- URL: `filer/mv`
- code: `mv.go`
- JSON-запрос:

	```
	{
	    "file": [
	        {
	            "path": [
	                {
	                    "id": 40393
	                } 
	            ]
	        }
	        
	        ,
	        {
	            "path": [
	                {
	                    "parentid": 40384,
	                    "name": "timestamp.zzz"
	                }
	            ]
	            
	        }
	        
	    ]
	}

	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/filer.sh mv 40393 40384 timestamp.zzz`


#### — `Rm`: удаление файлового объекта

- URL: `filer/rm`
- code: `rm.go`
- JSON-запрос:

	```
	{
	    "file": [
	        {
	            "path": [
	                {
	                    "id": 40393
	                } 
	            ]
	        }
	        
	    ]
	}

	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/filer.sh rm 40393`



### `permitter`

#### — `Show`: просмотр прав файлового объекта

- URL: `permitter/show`
- code: `show.go`
- JSON-запрос:

	```
	{
	    "file": [
	        {
	            "path": [
	                {
	                    "id": 40391
	                }
	            ]
	        }
	    ]
	}

	```
- JSON-ответ: 

	```
	{
	  "Data": [
	    {
	      "User": "test",
	      "Group": "",
	      "Permission": "Delete"
	    },
	    {
	      "User": "",
	      "Group": "test",
	      "Permission": "Modify"
	    }
	  ],
	  "Error": ""
	}

	```
- утилита: `test/permitter.sh show 40391`


#### — `Give`: предоставление прав файловому объекту

- URL: `permitter/give`
- code: `give_take.go`
- JSON-запрос:

	```
	{
	    "user": {
	        "name": "test"
	    },
	    "file": [
	        {
	            "path": [
	                {
	                    "id": 40391
	                }
	            ]
	        }
	    ],
	    "perm": [
	        {
	            "name": "Delete"
	        }
	    ]
	}
	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/permitter.sh give user test 40391 Delete`


#### — `Take`: удаление прав у файлового объекта

- URL: `permitter/take`
- code: `give_take.go`
- JSON-запрос:

	```
	{
	    "group": {
	        "name": "test"
	    },
	    "file": [
	        {
	            "path": [
	                {
	                    "id": 40391
	                }
	            ]
	        }
	    ],
	    "perm": [
	        {
	            "name": "Modify"
	        }
	    ]
	}
	```
- JSON-ответ: `{ "Data": null, "Error": "" }`
- утилита: `test/permitter.sh take group test 40391 Modify`



## `Request` 

Структура `Request` (`core/storage/route/data.go`) используется как основа для:
- JSON-запросов для взаимодействия с внешними сервисами;
- внутрисервисного взаимодействия через RPC.


`Request` c включенными развернутыми дочерними структурами и преобразованная в JSON-данные выглядит сл. образом, где:

- `struct`: дочерняя структура
- `[]struct`: массив из дочерних структур

 
```
Request 	struct {
    Auth    	struct {
				 	User struct {
						Id       int
						Name     string
						Password string
				 	}
				 	Group []struct {
						Id   int
						Name string
				 	}
				 	Token string
			  	}
    User 	  	struct {
				 	Id       int
				 	Name     string
					Password string
			  	}
    Group   	struct {
					Id   int
					Name string
			  	}
    File    	[]struct {
				 	Path []struct {
				 		File []struct {
							Id       int
							ParentId int
							Name     string
							Size     int64
							IsDir    bool
						}
				 	}
			  	}
	Perm 		[]struct {
					Id   int
				 	Name string
    		  	}
	Options 	struct {
    				Recursive bool
    			 	Offset    int
    			 	Limit     int
				}
}

```

