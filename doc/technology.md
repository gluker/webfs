# используемые технологии


### [github.com/valyala/gorpc](https://github.com/valyala/gorpc) 

Реализация коммуникации между микросервисами со следующими преимуществами перед стандартной реализацией `net/rpc`:

- автоматическое управление таймаутами, переподключениями, очередями сообщений;
- поддержка обмена безответными сообщениями, статистики, компрессии данных.


### [github.com/jinzhu/gorm](https://github.com/jinzhu/gorm)

ORM (Object-Relational Mapping) для Golang:

- связывание объектов ООП со структурой данных в СУБД;
- поддержка создания и управления структурами данных СУБД;
- возможность "бесшовного" перехода от одной СУБД к другой.


### [github.com/gorilla/mux](https://github.com/gorilla/mux)

HTTP request multiplexer с упрощенной организацией обработки HTTP запросов


### [github.com/google/uuid](https://github.com/google/uuid)

Генерация UUID для поддержки сессионного механизма авторизации.


### [stringer](https://github.com/golang/tools/blob/master/cmd/stringer/stringer.go)

Автоматизация создания именованных констант для разрешений в `core/storage/perm/data.go`



