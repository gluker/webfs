package indexer

import (
	"github.com/jinzhu/gorm"

	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/util"
)

var (
	Tx         *gorm.DB       // DB transation query handler
	lowDb      gorm.SQLCommon // DB low-level handler
	lowSql     string
	vals       []interface{}
	count      int
	current_id int
	psql_count int
)

const sqlStr string = "INSERT INTO file(parent_id, name, size, is_dir) VALUES"

func beginBulkInsert() {
	if storage.SqlDriver == "sqlite3" {
		storage.Db.Exec("PRAGMA synchronous = OFF")
	}

	Tx = storage.Db.Begin()
}

func commitBulkInsert() {
	if len(vals) > 0 {
		lowDb = Tx.CommonDB()
		lowSql = lowSql[0 : len(lowSql)-1]

		stmt, err := lowDb.Prepare(sqlStr + lowSql)
		util.FatalError(err)

		_, err = stmt.Exec(vals...)
		util.FatalError(err)
	}

	if err := Tx.Commit().Error; err != nil {
		util.FatalError(err)
	}

	if storage.SqlDriver == "sqlite3" {
		storage.Db.Exec("PRAGMA synchronous = FULL")
	}
}

func dbAddFile(parentId int, name string, size int64, isdir bool) int {
	count = count + 1
	if count > 1000 {
		commitBulkInsert()
		beginBulkInsert()
		lowSql = ""
		vals = []interface{}{}
		count = 0
		psql_count = 0
	}

	f := file.File{ParentId: parentId, Name: name, Size: size, IsDir: isdir}
	if isdir || storage.SqlDriver == "sqlite3" {
		if err := Tx.Save(&f).Error; err != nil {
			Tx.Rollback()
			util.FatalError(err)
		}
		current_id = f.Id
	} else {
		if storage.SqlDriver == "mysql" {
			lowSql += util.Sprintf("%s,", "(?,?,?,?)")
		} else {
			lowSql += util.Sprintf("($%d,$%d,$%d,$%d),",
				psql_count+1, psql_count+2, psql_count+3, psql_count+4)
			psql_count = psql_count + 4
		}
		val := []interface{}{f.ParentId, f.Name, f.Size, f.IsDir}
		vals = append(vals, val...)
	}

	return current_id
}
