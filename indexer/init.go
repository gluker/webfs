package indexer

import (
	"context"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/gluker/webfs/core/config"
	"gitlab.com/gluker/webfs/core/service"
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/util"
)

type IndexerLocal struct{}
type IndexerRemote struct{}

func init() {
	service.Register("indexer", &IndexerLocal{}, &IndexerRemote{})
}

func (r *IndexerRemote) Null() {}

func (l *IndexerLocal) Init(ctx context.Context) error {
	index, err := config.GetBoolErr("Index")
	if err != nil || !index {
		util.Verbose(util.ClearString())
		return err
	}

	configPath, err := filepath.Abs(filepath.Dir(
		config.GetString("Path") + "/"))
	util.FatalError(err)

	go func() {
		// start timing for indexing
		start := time.Now()

		// enabling mode for large inserts
		beginBulkInsert()

		// flush table and create startup root entry
		storage.Db.Delete(&file.File{})
		storage.Db.Save(&file.File{Id: 1, ParentId: 1, IsDir: true})

		// storage for parent_id
		parent := make(map[string]int)
		parent[configPath] = 1
		parent[filepath.Dir(configPath)] = 1

		// counting for files
		count := 0

		// DB add
		walkFunc := func(path string, info os.FileInfo, err error) error {
			if path == configPath {
				return nil
			}
			util.FatalError(err)

			if parent[filepath.Dir(path)] == 1 && info.Name() == "webfs.db" {
				return nil
			}

			count = count + 1
			id := dbAddFile(parent[filepath.Dir(path)],
				info.Name(), info.Size(), info.IsDir())
			if info.IsDir() {
				parent[path] = id
			}
			return nil
		}

		// filesystem walker
		err := filepath.Walk(configPath, walkFunc)
		util.FatalError(err)

		// commit db
		commitBulkInsert()

		// end timing for indexing
		end := time.Now()

		// banner for indexing
		util.Verbose(": indexing done for ", count,
			" objects in ", end.Sub(start), "\n")

		/* TOO SLOW
		// start timing for permitting
		start = time.Now()

		// enabling default permissions
		enablePermission()

		// end timing for indexing
		   end = time.Now()

		// banner for permitting
		util.Verbose("permitting done in ", end.Sub(start), "\n")
		*/
	}()

	return nil
}
