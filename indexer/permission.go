package indexer

import (
	"gitlab.com/gluker/webfs/core/rpc"
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/perm"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

type _ids struct {
	Id    int
	IsDir bool
}

var req route.Request

func init() {
	f := file.Data{}
	f.Path = append(f.Path, file.File{})
	req.File = append(req.File, &f)
	req.Perm = append(req.Perm, perm.Permission{})
	req.Group.Name = "everyone"
}

func setPermission(id int, isDir bool) {
	if isDir {
		req.Perm[0].Name = "List"
	} else {
		req.Perm[0].Name = "Read"
	}

	req.File[0].Path[0].Id = id

	_, err := rpc.RemoteClient["permitter"].Call("Give", &req)
	util.FatalError(err)
}

func enablePermission() {
	var ids []_ids

	err := storage.Db.Table("file").Select("id, is_dir").Find(&ids).Error
	util.FatalError(err)

	for i, _ := range ids {
		setPermission(ids[i].Id, ids[i].IsDir)
	}
}
