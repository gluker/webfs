#!/usr/bin/env bash

source `dirname $0`/init.sh

function usage {
    cat << EOF
Usage: $0 ParentId RemoteFilename LocalFilename

To upload LocalFilename as RemoteFilename to WebFS directory with Id = ParentId 

Example:
    $0 1 newfilename /etc/passwd 
EOF
    exit
}

function prepare {
    cat << EOF > upload.json
{
    "file": [
        {
            "path": [
                {
                    "parentid": ${1},
                    "name": "${2}"
                } 
            ]
        }    
    ]
}
EOF
}

function req {
    curl                                            \
        --dump-header -                             \
        --location                                  \
        --header "Content-Type: multipart/mixed"    \
        --request POST                              \
        --cookie cookie.txt                         \
        --form "json=@upload.json"                  \
        --form "data=@${1}"                         \
        ${PROTO}${WEBFS}/filer/upload
}

if [ ! $1 ] ; then
    usage
fi

prepare $1 $2

req $3

echo

