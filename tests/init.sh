#!/bin/sh

if [ ! $WEBFS ] ; then
    export WEBFS="localhost:8000"
    export WEBFS="webfs.gluk.su"
    export PROTO="https://"
    echo "init.sh: setting proto://hostname:port to ${PROTO}${WEBFS}"
fi
