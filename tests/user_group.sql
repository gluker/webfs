INSERT INTO "user" VALUES(1,'admin','$2a$10$wvBtXt4frvhP26K50c4qi.s1DSt4vEndtGHraThCSFFjCPl4iAYb.');
INSERT INTO "user" VALUES(2,'user','$2a$10$orAxiv7LaBREXyE7muXFZOj7bm7Js8XgudxDmLx2R8hW7pXMu0GE.');
INSERT INTO "group" VALUES(1,'admin');
INSERT INTO "group" VALUES(2,'user');
INSERT INTO "user_group" VALUES(1,1,1);
INSERT INTO "user_group" VALUES(2,1,2);
INSERT INTO "user_group" VALUES(3,2,2);
