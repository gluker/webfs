#!/usr/bin/env bash

source `dirname $0`/init.sh

function usage {
    cat << EOF
Usage: $0 Id LocalFilename

To download file with Id as LocalFilename

EOF
    exit
}

function prepare {
    cat << EOF > download.json
{
    "file": [
        {
            "path": [
                {
                    "id": $1
                } 
            ]
        }
    ]
}
EOF
    return
}

function req {
    curl                                                \
        --dump-header -                                 \
        --location                                      \
        --data @download.json                           \
        --header "Content-Type: application/json"       \
        --request POST                                  \
        --cookie cookie.txt                             \
        --output ${1}                                   \
        ${PROTO}${WEBFS}/filer/download
}

if [ ! $1 ] ; then
    usage
fi

prepare ${1}

req ${2} 

echo

