#!/usr/bin/env bash

source `dirname $0`/init.sh

function say {
    echo -e "\n\n*** ${1}"
}

function json {
    cat << EOF > login.json
{
    "auth": {
        "user": {
            "name": "$1",
            "password": "$2"
        }
    }
}
EOF
}

function req {
    say $1

    curl                                                \
        --dump-header -                                 \
        --location                                      \
        --data @login.json                              \
        --header "Content-Type: application/json"       \
        --request POST                                  \
        --cookie-jar cookie.txt                         \
        ${PROTO}${WEBFS}/authentificater/${1}
}

json $2 $3

req $1

echo
