#!/usr/bin/env bash

source `dirname $0`/init.sh

function say {
    echo -e "\n\n*** ${1}"
}

function prepare_show {
    cat << EOF > permitter.json
{
    "file": [
        {
            "path": [
                {
                    "id": $1
                }
            ]
        }
    ]
}
EOF
}


function prepare_json {
    cat << EOF > permitter.json
{
    "$1": {
        "name": "$2"
    },
    "file": [
        {
            "path": [
                {
                    "id": $3
                }
            ]
        }
    ],
    "perm": [
        {
            "name": "$4"
        }
    ]
}
EOF
}

function req {
    say $1

    curl                                                \
        --dump-header -                                 \
        --location                                      \
        --data @permitter.json                          \
        --header "Content-Type: application/json"       \
        --request POST                                  \
        --cookie cookie.txt                             \
        ${PROTO}${WEBFS}/permitter/${1}
}


if [ ! $1 ]; then
    echo `basename $0` action user\|group name file_id permission 
    exit
fi

if [ $1 = "show" ] ; then
    prepare_show ${2}
else 
    prepare_json ${2} ${3} ${4} ${5}

fi

req ${1} 

echo

