#!/usr/bin/env bash

source `dirname $0`/init.sh

function say {
    echo -e "\n\n*** ${1}"
}

function prepare_ls {
    if [ $1 ] ; then
        id=$1
    else
        id=1
    fi

    if [ $2 ] ; then
        limit=$2
    else
        limit=0
    fi

    if [ $3 ] ; then
        offset=$3
    else
        offset=0
    fi

    cat << EOF > filer.json
{
    "file": [
        {
            "path": [
                {
                    "id": $id
                } 
            ]
        }
        $add
    ],
    "options": {
        "limit": $limit,
        "offset": $offset
    }
}
EOF
    return
}

function prepare_mkdir {
    cat << EOF > filer.json
{
    "file": [
        {
            "path": [
                {
                    "parentid": $1,
                    "name": "${2}"
                } 
            ]
        }
    ]
}
EOF
    return
}


function prepare_json {
    if [ $2 ] ; then
        add="
        ,
        {
            \"path\": [
                {
                    \"parentid\": $2,
                    \"name\": \"$3\"
                }
            ]
            
        }
        "
    fi

    cat << EOF > filer.json
{
    "file": [
        {
            "path": [
                {
                    "id": $1
                } 
            ]
        }
        $add
    ]
}
EOF
    return
}

function req {
    say $1

    curl                                                \
        --dump-header -                                 \
        --location                                      \
        --data @filer.json                              \
        --header "Content-Type: application/json"       \
        --request POST                                  \
        --cookie cookie.txt                             \
        ${PROTO}${WEBFS}/filer/${1}
}


if [ $1 = "ls" ] ; then
    prepare_ls ${2} ${3} ${4}
else if [ $1 = "mkdir" ] ; then
    prepare_mkdir ${2} ${3}
    else
        prepare_json ${2} ${3} ${4}
    fi
fi

req ${1} 

echo

