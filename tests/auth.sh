#!/usr/bin/env bash

source `dirname $0`/init.sh

function say {
    echo -e "\n\n*** ${1}"
}


function json_name {
    cat << EOF > auth.json
{
    "$1": {
        "name": "$2",
        "password": "$3"
    }
}
EOF
}

function json_pass {
    cat << EOF > auth.json
{
    "user": {
        "name": "$1",
        "password": "$2"
    }
}
EOF
}


function json_both {
    cat << EOF > auth.json
{
    "group": {
        "name": "$1"
    },
    "user": {
        "name": "$2"
    }
}
EOF
}


function req {
    say $1

    curl                                                \
        --dump-header -                                 \
        --location                                      \
        --data @auth.json                               \
        --header "Content-Type: application/json"       \
        --request POST                                  \
        --cookie cookie.txt                             \
        ${PROTO}${WEBFS}/authentificater/${1}
}

if [[ $1 =~ "group-user" ]] ; then
    json_both $2 $3
else
    if [[ $1 =~ "user" ]] ; then
        json_name user $2 $3
    else
        json_name group $2
    fi

    if [[ $1 =~ "password" ]] ; then
        json_pass $2 $3
    fi
fi

req ${1}

echo

