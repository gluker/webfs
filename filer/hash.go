package filer

import (
	"crypto/md5"
	"crypto/sha256"
	"io/ioutil"

	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

type Hash struct {
	file.File
	Md5    string
	Sha256 string
}

// Md5 and Sha256 hash for given file
func (r *FilerRemote) Hash(req *route.Request) ([]Hash, error) {
	if req.File[0].Path[0].IsDir {
		return nil, util.Error("hash function can't apply for directory")
	}

	data, err := ioutil.ReadFile(req.File[0].Location())
	if err != nil {
		return nil, err
	}

	hash := make([]Hash, 1)
	hash[0].Id = req.File[0].Path[0].Id
	hash[0].ParentId = req.File[0].Path[0].ParentId
	hash[0].Name = req.File[0].Path[0].Name
	hash[0].Size = req.File[0].Path[0].Size
	hash[0].Md5 = util.Sprintf("%x", md5.Sum(data))
	hash[0].Sha256 = util.Sprintf("%x", sha256.Sum256(data))

	return hash, nil
}
