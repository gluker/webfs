package filer

import (
	"os"

	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func renameAndCheck(src, dst string) (err error) {
	if err = os.Link(src, dst); err != nil {
		return err
	}

	err = os.Remove(src)

	return err
}

func (r *FilerRemote) Mv(req *route.Request) (null, err error) {
	// locking source for Write
	err = Lock.Set(req.File[0].Path[0].Id, Write)
	defer Lock.Del(req.File[0].Path[0].Id, err)
	if err != nil {
		return null, err
	}

	// locking destination parent for Write
	err = Lock.Set(req.File[1].Path[0].ParentId, Write)
	defer Lock.Del(req.File[1].Path[0].ParentId, err)
	if err != nil {
		return null, err
	}

	var file file.File
	err = storage.Db.Where("id = ?", &req.File[0].Path[0].Id).Find(&file).Error
	if !util.CheckError(err) {
		return null, err
	}

	file.ParentId = req.File[1].Path[0].ParentId
	file.Name = req.File[1].Path[0].Name

	tx := storage.Db.Begin()

	err = tx.Save(&file).Error
	if !util.CheckError(err) {
		tx.Rollback()
		return null, err
	}

	err = renameAndCheck(req.File[0].Location(), req.File[1].Location())
	if !util.CheckError(err) {
		tx.Rollback()
		return null, err
	}

	err = tx.Commit().Error
	if !util.CheckError(err) {
		tx.Rollback()
	}

	return null, err
}
