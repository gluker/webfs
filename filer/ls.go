package filer

import (
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

// List files of given directory
func (r *FilerRemote) Ls(req *route.Request) (files []file.File, err error) {
	if !req.File[0].Path[0].IsDir && req.File[0].Path[0].Id > 1 {
		files = append([]file.File{req.File[0].Path[0]}, files...)
		return files, nil
	}

	if !req.File[0].Path[0].IsDir && req.File[0].Path[0].Id != 1 {
		return nil, nil
	}

	err = storage.Db.
		Limit(req.Options.Limit).
		Offset(req.Options.Offset).
		Where("id != 1").
		Where("parent_id = ?", req.File[0].Path[0].Id).
		Order("name").
		Find(&files).Error
	if !util.CheckError(err) {
		return nil, err
	}

	// adding path up
	if req.File[0].Path[0].Id != 1 {
		files = append([]file.File{req.File[0].Path[0]}, files...)
		files[0].Name = ".."
	}

	return files, nil
}
