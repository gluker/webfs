package filer

import (
	"sync"

	"gitlab.com/gluker/webfs/core/util"
)

// Read/Write Lock
type RWLock uint8

const (
	Read RWLock = iota + 1
	Write
)

func (l RWLock) String() string {
	if l == Read {
		return "Read"
	}
	if l == Write {
		return "Write"
	}
	return ""
}

type Status struct {
	lock  RWLock
	count uint16
}

// concurrent structure for file lock status
type ConcurrentLock struct {
	sync.RWMutex
	status map[int]Status
}

func NewConcurrentLock() *ConcurrentLock {
	return &ConcurrentLock{
		status: make(map[int]Status),
	}
}

func (l *ConcurrentLock) Get(id int) RWLock {
	l.RLock()
	defer l.RUnlock()

	if status, ok := l.status[id]; ok {
		return status.lock
	}

	return 0
}

func (l *ConcurrentLock) Set(id int, lock RWLock) error {
	var status Status
	var ok bool

	l.Lock()
	defer l.Unlock()

	if status, ok = l.status[id]; !ok {
		status.lock = lock
		status.count = 1
		l.status[id] = status
		return nil
	}

	if status.lock == Read {
		if lock == Read {
			status.count++
			l.status[id] = status
			return nil
		}
	}

	return util.Error("can't lock id: ", id, " for ", lock.String(),
		", it's already locked for ", status.lock.String())
}

func (l *ConcurrentLock) Del(id int, err error) {
	if err != nil {
		return
	}

	l.Lock()
	defer l.Unlock()

	if status, ok := l.status[id]; ok {
		status.count--
		if status.count == 0 {
			delete(l.status, id)
		} else {
			l.status[id] = status
		}
	}
}

// global storage for locks
var Lock *ConcurrentLock

func init() {
	Lock = NewConcurrentLock()
}
