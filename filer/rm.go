package filer

import (
	"os"

	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func (r *FilerRemote) Rm(req *route.Request) (null, err error) {
	// locking destination parent for Write
	err = Lock.Set(req.File[0].Path[0].Id, Write)
	defer Lock.Del(req.File[0].Path[0].Id, err)
	if err != nil {
		return null, err
	}

	tx := storage.Db.Begin()

	err = tx.Delete(&req.File[0].Path[0]).Error
	if !util.CheckError(err) {
		tx.Rollback()
		return null, err
	}

	err = os.Remove(req.File[0].Location())
	if !util.CheckError(err) {
		tx.Rollback()
		return null, err
	}

	err = tx.Commit().Error
	if !util.CheckError(err) {
		tx.Rollback()
	}

	return null, err
}
