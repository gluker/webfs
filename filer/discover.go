package filer

import (
	"gitlab.com/gluker/webfs/core/storage/perm"
	"gitlab.com/gluker/webfs/core/storage/route"
)

func (r *FilerRemote) Discover(i int) (
	fd []route.FunctionData, err error) {

	fd = append(fd, route.FunctionData{"Ls", []perm.Perm{perm.List}})
	fd = append(fd, route.FunctionData{"Rm", []perm.Perm{perm.Delete}})
	fd = append(fd, route.FunctionData{"Hash", []perm.Perm{perm.Read}})
	fd = append(fd, route.FunctionData{"Mkdir", []perm.Perm{perm.Create}})
	fd = append(fd, route.FunctionData{"Cp",
		[]perm.Perm{perm.Read, perm.Create}})
	fd = append(fd, route.FunctionData{"Mv",
		[]perm.Perm{perm.Delete, perm.Create}})

	fd = append(fd, route.FunctionData{"Upload", []perm.Perm{perm.Create}})
	fd = append(fd, route.FunctionData{"Download", []perm.Perm{perm.Read}})

	// lock testing
	fd = append(fd, route.FunctionData{"LockGet", []perm.Perm{perm.Read}})
	fd = append(fd, route.FunctionData{"LockDel", []perm.Perm{perm.Delete}})
	fd = append(fd, route.FunctionData{"LockSetRead",
		[]perm.Perm{perm.Create}})
	fd = append(fd, route.FunctionData{"LockSetWrite",
		[]perm.Perm{perm.Create}})

	return fd, nil
}
