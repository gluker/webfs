package filer

import (
	"io/ioutil"
	"os"

	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func (r *FilerRemote) Upload(req *route.Request) (null, err error) {
	err = Lock.Set(req.File[0].Path[0].ParentId, Write)
	defer Lock.Del(req.File[0].Path[0].ParentId, err)
	if err != nil {
		return null, err
	}

	file := file.File{
		ParentId: req.File[0].Path[0].ParentId,
		Name:     req.File[0].Path[0].Name,
		Size:     int64(len(req.Options.Data)),
		IsDir:    false}

	tx := storage.Db.Begin()
	err = tx.Save(&file).Error
	if !util.CheckError(err) {
		tx.Rollback()
		return null, err
	}

	err = ioutil.WriteFile(req.File[0].Location(), req.Options.Data, 0644)
	if !util.CheckError(err) {
		tx.Rollback()
		return null, err
	}

	err = tx.Commit().Error
	if !util.CheckError(err) {
		tx.Rollback()
		os.RemoveAll(req.File[0].Location())
	}

	return null, err
}
