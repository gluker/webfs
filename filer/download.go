package filer

import (
	"io/ioutil"
//	"os"

//	"gitlab.com/gluker/webfs/core/storage"
//	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/route"
//	"gitlab.com/gluker/webfs/core/util"
)

func (r *FilerRemote) Download(req *route.Request) (*route.Request, error) {
	err := Lock.Set(req.File[0].Path[0].Id, Read)
	defer Lock.Del(req.File[0].Path[0].Id, err)
	if err != nil {
		return req, err
	}

	req.Options.Data, err = ioutil.ReadFile(req.File[0].Location())

	return req, err
}
