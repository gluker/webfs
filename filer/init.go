package filer

import (
	"context"

	"gitlab.com/gluker/webfs/core/service"
)

type FilerLocal struct{}
type FilerRemote struct{}

func init() {
	service.Register("filer", &FilerLocal{}, &FilerRemote{})
}

func (l *FilerLocal) Init(ctx context.Context) error { return nil }
