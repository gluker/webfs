package filer

import (
	"bufio"
	"io"
	"os"

	"gitlab.com/gluker/webfs/core/rpc"
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/perm"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

// checking filetypes for source and destination
func CheckFile(src, dst string) (err error) {
	sfi, err := os.Stat(src)
	if err != nil {
		return err
	}

	if !sfi.Mode().IsRegular() {
		return util.Error("non-regular destination file: ", sfi.Name(),
			" (", sfi.Mode().String(), ")")
	}

	dfi, err := os.Stat(dst)
	if err != nil {
		if !os.IsNotExist(err) {
			return err
		}
	} else {
		if !(dfi.Mode().IsRegular()) {
			return util.Error("non-regular destination file: ", dfi.Name(),
				" (", dfi.Mode().String(), ")")
		}
		if os.SameFile(sfi, dfi) {
			return util.Error("source and destination are the same files")
		}
	}

	return nil
}

// filesystem copy operation
func CopyFile(src, dst string) (err error) {
	if err = CheckFile(src, dst); err != nil {
		return err
	}

	fi, err := os.Open(src)
	if err != nil {
		return err
	}
	defer fi.Close()

	fo, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer fo.Close()

	r := bufio.NewReader(fi)
	w := bufio.NewWriter(fo)
	buf := make([]byte, 8192)
	for {
		n, err := r.Read(buf)
		if err != nil && err != io.EOF {
			return err
		}

		if n == 0 {
			break
		}

		if _, err := w.Write(buf[:n]); err != nil {
			return err
		}
	}

	if err = w.Flush(); err != nil {
		return err
	}

	return nil
}

// copy file
func (r *FilerRemote) Cp(req *route.Request) (null, err error) {
	// locking source for Read
	err = Lock.Set(req.File[0].Path[0].Id, Read)
	defer Lock.Del(req.File[0].Path[0].Id, err)
	if err != nil {
		return null, err
	}

	// locking destination parent for Write
	err = Lock.Set(req.File[1].Path[0].ParentId, Write)
	defer Lock.Del(req.File[1].Path[0].ParentId, err)
	if err != nil {
		return null, err
	}

	// preparing destination
	file := file.File{
		ParentId: req.File[1].Path[0].ParentId,
		Name:     req.File[1].Path[0].Name,
		Size:     req.File[0].Path[0].Size}

	// preparing db entry for destination
	tx := storage.Db.Begin()
	err = tx.Save(&file).Error
	if !util.CheckError(err) {
		tx.Rollback()
		return null, err
	}

	// filesystem copying
	err = CopyFile(req.File[0].Location(), req.File[1].Location())
	if !util.CheckError(err) {
		tx.Rollback()
		return null, err
	}

	// committing changes to db
	err = tx.Commit().Error
	if !util.CheckError(err) {
		tx.Rollback()
		return null, err
	}

	// Full permission for destination for user who called copying
	req.User = req.Auth.User
	req.File[0].Path[0].Id = file.Id
	req.Perm = make([]perm.Permission, 1)
	req.Perm[0].Name = "Full"
	_, err = rpc.RemoteClient["permitter"].Call("Give", req)

	return null, err
}
