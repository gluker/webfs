package filer

import (
	"os"

	"gitlab.com/gluker/webfs/core/rpc"
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/file"
	"gitlab.com/gluker/webfs/core/storage/perm"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func (r *FilerRemote) Mkdir(req *route.Request) (null, err error) {
	// locking parent for Write
	err = Lock.Set(req.File[0].Path[0].ParentId, Write)
	defer Lock.Del(req.File[0].Path[0].ParentId, err)
	if err != nil {
		return null, err
	}

	// preparing new directory
	file := file.File{
		ParentId: req.File[0].Path[0].ParentId,
		Name:     req.File[0].Path[0].Name,
		IsDir:    true}

	// preparing db entry for new directory
	tx := storage.Db.Begin()
	err = tx.Save(&file).Error
	if !util.CheckError(err) {
		tx.Rollback()
		return null, err
	}

	// filesystem mkdir
	err = os.Mkdir(req.File[0].Location(), 0755)
	if !util.CheckError(err) {
		tx.Rollback()
		return null, err
	}

	// committing changes to db
	err = tx.Commit().Error
	if !util.CheckError(err) {
		tx.Rollback()
		os.RemoveAll(req.File[0].Location())
		return null, err
	}

	// Full permission for directory for user who called mkdir
	req.User = req.Auth.User
	req.File[0].Path[0].Id = file.Id
	req.Perm = make([]perm.Permission, 1)
	req.Perm[0].Name = "Full"
	_, err = rpc.RemoteClient["permitter"].Call("Give", req)

	return null, err
}
