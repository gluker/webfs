package filer

import "gitlab.com/gluker/webfs/core/storage/route"

// remote function for checking locks
type Ret struct {
	Lock string
}

// returns lock status
func (r *FilerRemote) LockGet(req *route.Request) (ret []Ret, err error) {
	lock := Lock.Get(req.File[0].Path[0].Id)
	ret = append(ret, Ret{Lock: lock.String()})
	return ret, nil
}

// set Read lock for file.Id
func (r *FilerRemote) LockSetRead(req *route.Request) (null, err error) {
	err = Lock.Set(req.File[0].Path[0].Id, Read)
	return null, err
}

// set Write lock for file.Id
func (r *FilerRemote) LockSetWrite(req *route.Request) (null, err error) {
	err = Lock.Set(req.File[0].Path[0].Id, Write)
	return null, err
}

// decrement lock count or removes lock for file.Id
func (r *FilerRemote) LockDel(req *route.Request) (null, err error) {
	Lock.Del(req.File[0].Path[0].Id, nil)
	return null, nil
}
