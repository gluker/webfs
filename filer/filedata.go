package filer

import (
	"gitlab.com/gluker/webfs/core/config"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func (r *FilerRemote) FileData(req *route.Request) (*route.Request, error) {
	var err error

	if config.GetBool("Chroot") && req.File[0].Path[0].ParentId == 1 &&
		req.File[0].Path[0].Name == "webfs.db" {
		return nil, util.Error("denied to process filename \"",
			req.File[0].Path[0].Name, "\" in ParentId ",
			req.File[0].Path[0].ParentId)
	}

	for i := 0; i < len(req.File); i++ {
		if err = req.File[i].Build(); err != nil {
			break
		}
	}

	return req, err
}
