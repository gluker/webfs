# WebFS
  
WebFS provides ability to work with given directory through
HTTP API to:

- create/copy/rename/delete/upload/download directories and files according to its permissions;
- manage permissions for directories and files linked to users and groups;
- administer users/groups for different types to access to WebFS directory;


## Example usage 

#### download/unarchive/prepare webfs
```
wget https://gitlab.fbsvc.bz/gluker/webfs/-/archive/master/webfs-master.tar.gz

tar xv webfs-master

cd webfs-master/tests
```

#### test webfs demo at http://webfs.gluk.su
```
export WEBFS=webfs.gluk.su

./login.sh login admin aaa123 

./auth.sh user-list

./filer.sh mkdir 1 xxx

./upload.sh 1 zzz /etc/passwd

./filer.sh ls
```


## API

See [doc/API.md](doc/API.md) for list of APIs
                                                                              
