package authentificater

import (
	"sync"

	"gitlab.com/gluker/webfs/core/storage/auth"
)

// concurrent protected storage with pointers
type AuthDataCache struct {
	sync.RWMutex
	cache map[string]*auth.Data
}

func NewAuthDataCache() *AuthDataCache {
	return &AuthDataCache{
		cache: make(map[string]*auth.Data),
	}
}

func (a *AuthDataCache) Load(key string) (authData *auth.Data, ok bool) {
	a.RLock()
	defer a.RUnlock()
	authData, ok = a.cache[key]
	return authData, ok
}

func (a *AuthDataCache) Store(key string, authData *auth.Data) {
	a.Lock()
	defer a.Unlock()
	a.cache[key] = authData
}

func (a *AuthDataCache) Delete(key string) {
	a.Lock()
	defer a.Unlock()
	delete(a.cache, key)
}

func (a *AuthDataCache) Flush() {
	a.Lock()
	defer a.Unlock()
	a.cache = make(map[string]*auth.Data)
}
