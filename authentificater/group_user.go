package authentificater

import (
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/auth"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func getUserGroup(req *route.Request) (err error) {
	authByUser.Flush()
	authByToken.Flush()

	if req.Group.Id == 0 {
		err = storage.Db.Where("name = ?", req.Group.Name).
			Find(&req.Group).Error
	}
	if !util.CheckError(err) {
		return err
	}

	if req.User.Id == 0 {
		err = storage.Db.Where("name = ?", req.User.Name).
			Find(&req.User).Error
	}

	util.CheckError(err)
	return err
}

func (r *AuthentificaterRemote) GroupUserAdd(req *route.Request) (
	null, err error) {

	if storage.SqlDriver == "postgres" {
		storage.Db.Exec("SELECT setval('user_group_id_seq'," +
			"(SELECT MAX(id) FROM \"user_group\"))")
	}

	if err = getUserGroup(req); err != nil {
		return null, err
	}

	ug := auth.UserGroup{UserId: req.User.Id, GroupId: req.Group.Id}
	err = storage.Db.Save(&ug).Error

	util.CheckError(err)
	return null, err
}

func (r *AuthentificaterRemote) GroupUserDelete(req *route.Request) (
	null, err error) {

	if err = getUserGroup(req); err != nil {
		return null, err
	}

	err = storage.Db.Delete(&auth.UserGroup{},
		"user_id = ? AND group_id = ?", req.User.Id, req.Group.Id).Error

	util.CheckError(err)
	return null, err
}
