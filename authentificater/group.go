package authentificater

import (
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/auth"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func (r *AuthentificaterRemote) GroupList(req *route.Request) (
	groupDatas []auth.GroupData, err error) {

	var groups []auth.Group
	if req.Group == (auth.Group{}) {
		err = storage.Db.
			Limit(req.Options.Limit).
			Offset(req.Options.Offset).
			Find(&groups).Error
	} else {
		if req.Group.Id != 0 {
			err = storage.Db.Where("id = ?", req.Group.Id).Find(&groups).Error
		} else {
			err = storage.Db.Where("name = ?", req.Group.Name).Find(&groups).
				Error
		}
	}
	if !util.CheckError(err) {
		return nil, err
	}

	for _, group := range groups {
		var users []auth.User
		err = storage.Db.Joins("JOIN user_group ON "+
			"user_group.user_id = \"user\".id").
			Where("user_group.group_id = ?", group.Id).
			Find(&users).Error
		if !util.CheckError(err) {
			return nil, err
		} else {
			groupDatas = append(groupDatas,
				auth.GroupData{Group: group, User: users})
		}
	}

	return groupDatas, nil
}

func (r *AuthentificaterRemote) GroupAdd(req *route.Request) (null, err error) {
	if storage.SqlDriver == "postgres" {
		storage.Db.Exec(
			"SELECT setval('group_id_seq', (SELECT MAX(id) FROM \"group\"))")
	}

	group := auth.Group{Name: req.Group.Name}
	err = storage.Db.Save(&group).Error

	util.CheckError(err)
	return null, err
}

func (r *AuthentificaterRemote) GroupDelete(req *route.Request) (
	null, err error) {

	authByUser.Flush()
	authByToken.Flush()

	err = storage.Db.Delete(&auth.Group{}, "name = ?", req.Group.Name).Error

	util.CheckError(err)
	return null, err
}
