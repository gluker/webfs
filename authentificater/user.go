package authentificater

import (
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/auth"
	"gitlab.com/gluker/webfs/core/storage/route"
	"gitlab.com/gluker/webfs/core/util"
)

func (r *AuthentificaterRemote) UserList(req *route.Request) (
	authDatas []auth.Data, err error) {

	var users []auth.User
	if req.User == (auth.User{}) {
		err = storage.Db.
			Limit(req.Options.Limit).
			Offset(req.Options.Offset).
			Find(&users).Error
	} else {
		if req.User.Id != 0 {
			err = storage.Db.Where("id = ?", req.User.Id).Find(&users).Error
		} else {
			err = storage.Db.Where("name = ?", req.User.Name).Find(&users).Error
		}
	}
	if !util.CheckError(err) {
		return nil, err
	}

	for _, user := range users {
		var groups []auth.Group
		err = storage.Db.Joins("JOIN user_group ON "+
			"user_group.group_id = \"group\".id").
			Where("user_group.user_id = ?", user.Id).
			Find(&groups).Error
		if !util.CheckError(err) {
			return nil, err
		} else {
			authDatas = append(authDatas, auth.Data{User: user, Group: groups})
		}
	}

	for i, data := range authDatas {
		if user, ok := authByUser.Load(data.User.Name); ok {
			authDatas[i].Token = user.Token
		}
	}

	return authDatas, nil
}

func (r *AuthentificaterRemote) UserAdd(req *route.Request) (null, err error) {
	hashedPassword, err := bcrypt.GenerateFromPassword(
		[]byte(req.User.Password), bcrypt.DefaultCost)
	if !util.CheckError(err) {
		return null, err
	}

	if storage.SqlDriver == "postgres" {
		storage.Db.Exec(
			"SELECT setval('user_id_seq', (SELECT MAX(id) FROM \"user\"))")
	}

	user := auth.User{Name: req.User.Name, Password: string(hashedPassword)}
	err = storage.Db.Save(&user).Error

	util.CheckError(err)
	return null, err
}

func (r *AuthentificaterRemote) UserDelete(req *route.Request) (
	null, err error) {

	if data, ok := authByUser.Load(req.User.Name); ok {
		authByUser.Delete(req.User.Name)
		authByToken.Delete(data.Token)
	}

	err = storage.Db.Delete(&auth.User{}, "name = ?", req.User.Name).Error

	util.CheckError(err)
	return null, err
}

func (r *AuthentificaterRemote) UserPassword(req *route.Request) (
	null, err error) {

	hashedPassword, err := bcrypt.GenerateFromPassword(
		[]byte(req.User.Password), bcrypt.DefaultCost)
	if !util.CheckError(err) {
		return null, err
	}

	if data, ok := authByUser.Load(req.User.Name); ok {
		data.User.Password = string(hashedPassword)
		authByUser.Store(req.User.Name, data)
		authByToken.Store(data.Token, data)
	}

	var user auth.User
	err = storage.Db.Where("name = ?", req.User.Name).Find(&user).Error
	if !util.CheckError(err) {
		return null, err
	}

	user.Password = string(hashedPassword)
	err = storage.Db.Save(&user).Error

	util.CheckError(err)
	return null, err
}
