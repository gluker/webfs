package authentificater

import (
	"context"

	"gitlab.com/gluker/webfs/core/service"
	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/auth"
	"gitlab.com/gluker/webfs/core/util"
)

type AuthentificaterLocal struct{}  // for local API calls
type AuthentificaterRemote struct{} // for remote (RPC) API calls

var (
	authByUser  *AuthDataCache // for faster search by user name
	authByToken *AuthDataCache // for faster search by token
)

func init() {
	service.Register("authentificater",
		&AuthentificaterLocal{}, &AuthentificaterRemote{})
}

func (l *AuthentificaterLocal) Init(ctx context.Context) error {
	authByUser = NewAuthDataCache()
	authByToken = NewAuthDataCache()

	// creating user 'admin' if db table user is empty
	var users []auth.User
	err := storage.Db.Find(&users).Error
	util.FatalError(err)

	if len(users) == 0 {
		err := storage.Db.Save(&auth.User{Name: "admin", Password: "$2a$10$wvBtXt4frvhP26K50c4qi.s1DSt4vEndtGHraThCSFFjCPl4iAYb."}).Error
		util.FatalError(err)
	}

	return nil
}
