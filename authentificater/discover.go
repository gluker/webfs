package authentificater

import (
	"gitlab.com/gluker/webfs/core/storage/perm"
	"gitlab.com/gluker/webfs/core/storage/route"
)

func (r *AuthentificaterRemote) Discover(i int) (
	fd []route.FunctionData, err error) {

	fd = append(fd, route.FunctionData{"UserList", []perm.Perm{}})
	fd = append(fd, route.FunctionData{"UserAdd", []perm.Perm{}})
	fd = append(fd, route.FunctionData{"UserDelete", []perm.Perm{}})
	fd = append(fd, route.FunctionData{"UserPassword", []perm.Perm{}})

	fd = append(fd, route.FunctionData{"GroupList", []perm.Perm{}})
	fd = append(fd, route.FunctionData{"GroupAdd", []perm.Perm{}})
	fd = append(fd, route.FunctionData{"GroupDelete", []perm.Perm{}})
	fd = append(fd, route.FunctionData{"GroupUserAdd", []perm.Perm{}})
	fd = append(fd, route.FunctionData{"GroupUserDelete", []perm.Perm{}})

	return fd, nil
}
