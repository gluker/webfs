package authentificater

import (
	"github.com/google/uuid"

	"golang.org/x/crypto/bcrypt"

	"gitlab.com/gluker/webfs/core/storage"
	"gitlab.com/gluker/webfs/core/storage/auth"
	"gitlab.com/gluker/webfs/core/util"
)

// check for login/password and update token
func (r *AuthentificaterRemote) Login(user *auth.User) (*auth.Data, error) {
	var authData *auth.Data

	if data, ok := authByUser.Load(user.Name); ok {
		err := bcrypt.CompareHashAndPassword(
			[]byte(data.User.Password), []byte(user.Password))
		if !util.CheckError(err) {
			return nil, util.Error("login/password mismatch")
		}

		// old token for user expired
		if _, ok = authByToken.Load(data.Token); ok {
			authByToken.Delete(data.Token)
		}

		// new token generation
		data.Token = uuid.New().String()

		// save new token to authByToken with pointer to auth.Data
		authByToken.Store(data.Token, data)

		// prepare auth.Data to return to requester
		authData = data
	} else { // find user info in DB
		// data will stay in memory and not get collected by GC
		// until it has a reference (through pointers) from maps in
		// authByUser and authByToken
		var data auth.Data

		err := storage.Db.Where("name = ?", user.Name).
			Find(&data.User).Error
		if !util.CheckError(err) {
			return nil, util.Error("login/password mismatch")
		}

		err = bcrypt.CompareHashAndPassword(
			[]byte(data.User.Password), []byte(user.Password))
		if !util.CheckError(err) {
			return nil, util.Error("login/password mismatch")
		}

		err = storage.Db.Joins("JOIN user_group ON "+
			"user_group.group_id = \"group\".id").
			Where("user_group.user_id = ?", data.User.Id).
			Find(&data.Group).Error
		if !util.CheckError(err) {
			return nil, err
		}

		// new token generation
		data.Token = uuid.New().String()

		// save pointer to auth.Data to in-memory maps
		authByUser.Store(data.User.Name, &data)
		authByToken.Store(data.Token, &data)

		// prepare auth.Data to return to requester
		authData = &data
	}

	return authData, nil
}

// Returns auth.Data for token
func (r *AuthentificaterRemote) Token(token string) (*auth.Data, error) {
	if data, ok := authByToken.Load(token); ok {
		return data, nil
	}

	return nil, util.Error("token not found")
}
